//
//  ViewController.swift
//  IOS_MD5_Guntis_Ozols
//
//  Created by Gman on 24/05/2018.
//  Copyright © 2018 Guntis Ozols. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, SecondViewControllerDelegate {
    
    func textUpdated(newText: String) {
        print(newText)
    }
    @IBOutlet weak var mapView: MKMapView!
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "showSecondController", let vc = segue.destination as? ViewController2{
            vc.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var lat1 = "", lon1 = "", nos1 = "", apr1 = ""
        var lat2 = "", lon2 = "", nos2 = "", apr2 = ""
        var lat3 = "", lon3 = "", nos3 = "", apr3 = ""
        
        if let toFile = Bundle.main.path(forResource: "MapLocations", ofType: "plist"){
            let array = NSArray(contentsOfFile: toFile)
            if let location = array {
                let value = location[0] as! NSDictionary
                lat1 = value["Lat"] as! String
                lon1 = value["Lon"] as! String
                nos1 = value["Nos"] as! String
                apr1 = value["Apr"] as! String
            }
            if let location = array {
                let value = location[1] as! NSDictionary
                lat2 = value["Lat"] as! String
                lon2 = value["Lon"] as! String
                nos2 = value["Nos"] as! String
                apr2 = value["Apr"] as! String
            }
            if let location = array {
                let value = location[2] as! NSDictionary
                lat3 = value["Lat"] as! String
                lon3 = value["Lon"] as! String
                nos3 = value["Nos"] as! String
                apr3 = value["Apr"] as! String
            }
        }
        let location1 = MKPointAnnotation()
        location1.coordinate = CLLocationCoordinate2D(latitude: Double(lat1)!, longitude: Double(lon1)!)
        location1.title = nos1
        location1.subtitle = apr1
        
        let location2 = MKPointAnnotation()
        location2.coordinate = CLLocationCoordinate2D(latitude: Double(lat2)!, longitude: Double(lon2)!)
        location2.title = nos2
        location2.subtitle = apr2
        
        let location3 = MKPointAnnotation()
        location3.coordinate = CLLocationCoordinate2D(latitude: Double(lat3)!, longitude: Double(lon3)!)
        location3.title = nos3
        location3.subtitle = apr3
        
        mapView.addAnnotation(location1)
        mapView.addAnnotation(location2)
        mapView.addAnnotation(location3)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


