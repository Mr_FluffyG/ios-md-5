//
//  ViewController2.swift
//  IOS_MD5_Guntis_Ozols
//
//  Created by Gman on 24/05/2018.
//  Copyright © 2018 Guntis Ozols. All rights reserved.
//

import UIKit

protocol SecondViewControllerDelegate {
    func textUpdated(newText: String)
}

class ViewController2: UIViewController {
    
    @IBAction func buttonTouch(_ sender: Any) {
        self.delegate?.textUpdated(newText: "wup")
    }
    
    var delegate: SecondViewControllerDelegate?
    
    @IBOutlet weak var filterSwitch: UISwitch!
    
    @IBAction func valueSaved(_ sender: UISwitch) {
        UserDefaults.standard.set(sender.isOn, forKey: "filterSwitchState")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filterSwitch.isOn =  UserDefaults.standard.bool(forKey: "filterSwitchState")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
